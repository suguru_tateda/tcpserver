#pragma once
#include <map>
///////////////////////////////////////////////////////////////////////////////
// typedef
///////////////////////////////////////////////////////////////////////////////

//関数ポインタ
//引数はハンドラ側の実装に委ねている。ワイルドカード状態なのでvoid*。void*にしておけばint*やlong*といった色々な型の引数を受け取れる。
//ポインタは8バイトというサイズが決まっているのでどの型にもキャストできる
//typedef int (*EVHANDLEER)(void* pParm);
//typedef std::map<int, EVHANDLEER> eventMap;

//C++11スタンダードでのType Alias
using EVHANDLEER = int(*)(void* pParm);
using eventMap = std::map<int, EVHANDLEER>;

//引数をテンプレートクラスにすると同時にusing型エイリアスでdictというmapを定義する
//template<class Value>
//using dict = std::map<std::string, Value>;

///////////////////////////////////////////////////////////////////////////////
// クラス定義
///////////////////////////////////////////////////////////////////////////////
class CMainLoop {
public:
	CMainLoop();			// コンストラクタ
	~CMainLoop();			// デストラクタ

	bool addHandler(int nEvent, EVHANDLEER handler);			// ハンドラ登録
	bool deleteHandler(int nEvent);								// ハンドラ削除

	bool addTimeoutHandler(int nTimeout, EVHANDLEER handler);	// ハンドラ登録(timeout用)
	bool deleteTimeoutHandler();								// ハンドラ削除(timeout用)

	bool mainLoop();											// メインループ開始
	bool exitMainLoop();										// メインループ終了

private:
	//int  _count;
	int  _nTimeout;							// タイムアウト時間(秒)
	bool _bLoopExit;						// メインループ終了フラグ
	eventMap	_eventMap;					// ハンドラ管理マップ
	EVHANDLEER  _timeOutHandler;			// タイムアウト用のハンドラポインタ
};