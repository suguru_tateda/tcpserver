///////////////////////////////////////////////////////////////////////////////
//  TcpServer
///////////////////////////////////////////////////////////////////////////////
#include <signal.h>
#include <errno.h>
#include <err.h>
#include <stdio.h>
#include "CMainLoop.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <arpa/inet.h>
#include "TcpCommon.h"
#define MAXFD 64
using namespace std;
///////////////////////////////////////////////////////////////////////////////
// プロトタイプ宣言
///////////////////////////////////////////////////////////////////////////////
int handler_1(void* pParm);
int handler_2(void* pParm);
int handler_timeout(void* pParm);
int handler_listen(void* pParm);
int handler_recv(void* pParm);
bool createServerSock(int nPortNo, int& nSock);
void sig_handler(int signo);
int recover_log();
///////////////////////////////////////////////////////////////////////////////
// 外部変数
///////////////////////////////////////////////////////////////////////////////
CMainLoop	mLoop;
int fd_start;//Startへの立ち上がり完了報告用名前付きパイプ;
int fd_stop;//Stopへの立ち上がり完了報告用名前付きパイプ;
//int fd_log;//ログ用fd
int fddevnull = 0;//dev/null用fd
int pp[2];//EventServer-Log間の立ち上がり連絡用名前無しパイプ
int logpipe[2];//EventServer-Log間のログ用名前無しパイプ
int log_pid;
int lRet = 0;
char log_path[255];

bool bRecoverLog = false;
int  fd_req = -1;
///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	//sleep(16);

	char work[256];
    pid_t s_pid;
	int nPortNo;

    //Startからポート番号を受け取る
    nPortNo = atoi(argv[1]);

    //Startからプロセスidを受け取る
	char* tmp = argv[2];

	//------------------------------------------------------
    // 状態.
    // 子プロセスのみとなったがTTYは保持したまま.
    //------------------------------------------------------


    //------------------------------------------------------
    // TTYを切り離してセッションリーダー化、プロセスグループリーダー化する.
    //------------------------------------------------------
    setsid();


    //------------------------------------------------------
    // HUPシグナルを無視.
    // 親が死んだ時にHUPシグナルが子にも送られる可能性があるため.
    //------------------------------------------------------
    signal(SIGHUP, SIG_IGN);
 	signal(SIGCHLD, SIG_IGN);


    //------------------------------------------------------
    // 状態.
    // このままだとセッションリーダーなのでTTYをオープンするとそのTTYが関連づけられてしまう.
    //------------------------------------------------------


    //------------------------------------------------------
    // 親プロセス(セッショングループリーダー)を切り離す.
    // 親を持たず、TTYも持たず、セッションリーダーでもない状態になる.
    //------------------------------------------------------
	pid_t pid = 0;
	pid = fork();
    if (pid == -1 ) 
	{
        DebugLogForDaemon("Fork has failed in EventServer.cpp\n");
		return -1;
    }

    if(pid != 0){
        // 子供世代プロセスを終了し、孫世代になる.
        _exit(0);
    }

    //------------------------------------------------------
    // デーモンとして動くための準備を行う.
    //------------------------------------------------------
	//Logファイルパス取得
	char dir[255];
	getcwd(dir,255);
	//char log_path[255];
	sprintf(log_path, "%s/Log", PROJ_HOME);


    // カレントディレクトリ変更.
    // ルートディレクトリに移動.(デーモンプロセスはルートディレクトリを起点にして作業するから)
    chdir("/");

    // 親から引き継いだ全てのファイルディスクリプタのクローズ.
    for(int i = 0; i < MAXFD; i++){
        close(i);
    }

    // stdin,stdout,stderrをdev/nullでオープン.
    // 単にディスクリプタを閉じるだけだとこれらの出力がエラーになるのでdev/nullにリダイレクトする.
    if((fddevnull = open("/dev/null", O_RDWR, 0) != -1)){

        // ファイルディスクリプタの複製.
        // このプロセスの0,1,2をfdが指すdev/nullを指すようにする.
        dup2(fddevnull, 0);
        dup2(fddevnull, 1);
        dup2(fddevnull, 2);
        if(fddevnull < 2){
            close(fddevnull);
        }
    }

    //------------------------------------------------------
    // デーモン化後の処理
    //------------------------------------------------------

	//エラー判定値(int)
	int nRet = 0;

    // シグナルハンドラの設定
    struct sigaction act;
    memset(&act, 0, sizeof(act)); //メモリにゴミが入っているので初期化
    act.sa_handler = sig_handler;
    act.sa_flags = SA_RESTART; //何度シグナルが来てもハンドラ実行を許可する
    //act.sa_restorer = NULL; //廃止予定。POSIXでも規定されていない

//0 or 1で簡単に取り外し可能
#if 1
    // 割り込みを抑止するシグナルの設定
    sigset_t sigset; // シグナルマスク
    
    //シグナルマスクの初期化
    nRet = sigemptyset(&sigset);
    if( nRet != 0 ) return -1;

    //Control-Cで割り込まれないようにする
    nRet = sigaddset(&sigset, SIGUSR1);
    if( nRet != 0 ) return -1;
    act.sa_mask = sigset;
#endif
    //第1引数はシステムコール番号
    //第2引数は第1引数で指定したシステムコールで呼び出したいアクション
    //第3引数は第1引数で指定したシステムコールでこれまで呼び出されていたアクションが格納される。NULLだとこれまでの動作が破棄される
    nRet = sigaction(SIGINT,
                    &act,
                    NULL);
    if ( nRet == -1 ) err(EXIT_FAILURE, "sigaction error");

	//SIGTERMを補足
	memset(&act, 0, sizeof(act));
	act.sa_handler = sig_handler;
	nRet = sigaction(SIGTERM,
				&act,
				NULL);

	//立ち上がり連絡用名前なしパイプ作成
	pipe(pp);
	char pp_buf[16];
	sprintf(pp_buf, "%d", pp[1]);//文字列に変換

	//ログ用名前なしパイプ作成
	pipe(logpipe);
	char logpipe_buf[1024];
	sprintf(logpipe_buf, "%d", logpipe[0]);//文字列に変換

	//ログプロセス起動
	pid_t e_pid = getpid(); //EventServerプロセスID取得
    char buff[16];
    sprintf(buff, "%d", e_pid);//文字列に変換

	pid = fork();

    if (pid == -1 ) 
	{
        DebugLogForDaemon("fork log has failed in EventServer.cpp\n");
		return -1;
    }
	else if (pid == 0) //子プロセスには0が返る
	{
		close(pp[0]); //名前無しパイプの読み込みディスクリプタをすぐ閉じる
		close(logpipe[1]);//ログ用名前なしパイプの書き込みをすぐ閉じる

		//複製した子プロセスをLogに化けさせる
		char* str[] = {(char*)log_path, buff, pp_buf, logpipe_buf, NULL};
		nRet = execv(log_path, str);
        if ( nRet == -1 ) 
		{
            DebugLogForDaemon("log execv has failed!\n");
			return -1;
        }
    }

	close(pp[1]); //名前無しパイプの書き込みディスクリプタを閉じる
	close(logpipe[0]);//ログ用名前なしパイプの読み込みディスクリプタをすぐ閉じる

    //Logプロセス立ち上がり完了報告を読取
    bool flag = true;
	int nbyte;
	char buf[16];

    while (flag == true){
        if((nbyte = read(pp[0], buf, 16)) > 0){
			log_pid = atoi(buf);
            flag = false;
        }else if(nbyte == -1){
			//perror("read()");
			DebugLogForDaemon("Cannot read log pipe\n");
			close(pp[0]);
			return -1;
		}
    }

	bool bRet = true;//エラー判定値(bool)

	// 要求受け付け用の名前付きパイプ
	unlink(REQUEST_PIPE);
	nRet = mkfifo(REQUEST_PIPE, 0666);
	if ( nRet ==-1 ) {
		//perror("mkfifo REQUEST_PIPE");
		DebugLogForDaemon("mkfifo REQUEST_PIPE failed\n");
		return -1;
	}
	fd_req = open(REQUEST_PIPE, O_RDWR);
	if ( fd_req == -1 ) {
		//perror("open REQUEST_PIPE");
		DebugLogForDaemon("cannot open REQUEST_PIPE\n");
		return -1;		
	}

	bRet = mLoop.addHandler(fd_req, handler_2);
	if ( bRet == false ) return -1;

	// listen用ソケットの生成
	int nSock = -1;
	bRet = createServerSock(nPortNo, nSock);

	/*
	// ハンドラ登録 (fifo)
	bRet = mLoop.addHandler(fd_log, handler_1);
	if ( bRet == false ) {
		//printf("addHandler error\n");
		return -1;
	}
	*/

	// ハンドラ登録 (listenソケット)
	bRet = mLoop.addHandler(nSock, handler_listen);
	if ( bRet == false ) return -1;


	// ハンドラ登録(タイムアウト用)
	bRet = mLoop.addTimeoutHandler(3, handler_timeout);
	if ( bRet == false ) return -1;

	//PIDファイル作成
	if(write_pid(buff, PID_SERVER) == false){
		//perror("write_pid()");
		DebugLogForDaemon("write PID_SERVER failed\n");
		return -1;
	};

    /* 書込専用でパイプを開く */
    if ((fd_start = open(PIPE_START, O_WRONLY)) == -1)
    {
        //perror("open()");
		DebugLogForDaemon("open PIPE_START failed\n");
        return -1;
    }

	/* 立ち上がり完了報告 プロセスid送信*/
	if (write(fd_start, buff, strlen(buff)) != strlen(buff))
	{
		//perror("write()");
		DebugLogForDaemon("open fd_start failed\n");
		close(fd_start);
		return -1;
	}

	//ログテスト送信
	lRet = logFunc(logpipe[1], EDebugLv::__DEBUG, e_pid, "StartUp Completed! %d", 100);
	if(lRet == -1){
		close(logpipe[1]);
		return -1;
	}

	// メインループ (終了要求受付まで制御がもどらない)
	bRet = mLoop.mainLoop();

	if ( bRet == false ) {
		/*
		// 入力ファイルのクローズ
		if ( fd_log != -1 ) {
			//TODO
			//ログプロセスにsignal送る -> ログプロセス終了 ->終了通知 ->パイプclose&unlink
			close(fd_log);
		}
		*/
		DebugLogForDaemon("mLoop.mainLoop() has failed\n");
		return -1;
	}

	//Logプロセスクローズ
	nRet = kill(log_pid, SIGUSR1);
	sprintf(work, "kill nRet=%d, log_pid=%d\n", nRet, log_pid);
	DebugLogForDaemon(work);
	//close(fd_log);
	//unlink(PIPE_LOG);

	//logプロセスからの終了報告待ち
    bool _flag = true;
	int _nbyte;
	char _buf[16];

	//(解消)EventServerプロセスとLogプロセスが両方ともStopコマンドで終了しなくなる
    while (_flag == true){
		DebugLogForDaemon("before read\n");

		_nbyte = read(pp[0], _buf, 16);
		char work[256];
		sprintf(work, "_nbyte = %d\n", _nbyte);
		DebugLogForDaemon(work);
        if( _nbyte  > 0 ) {
            _flag = false;
        }else if( _nbyte == -1) {
			//perror("read()");
			DebugLogForDaemon("read pp failed\n");
			close(pp[0]);
			close(logpipe[1]);
			return -1;
		}
		else if ( _nbyte == 0 ) {
			DebugLogForDaemon("EventServer read error\n");
			break;
		}
		DebugLogForDaemon("after read\n");
    }
	
	//パイプクローズ
	close(pp[0]);
	close(logpipe[1]);
	if (close(fd_req) != 0) DebugLogForDaemon("close fd_req failed\n");
	unlink(REQUEST_PIPE);
	
	//Stopコマンドへ名前付きパイプで終了完了報告
	unlink(PID_SERVER);
	
    // 書込専用でパイプを開く
    if ((fd_stop = open(PIPE_STOP, O_WRONLY)) == -1)
    {
		DebugLogForDaemon("Cannot Open fd_stop\n");
		//perror("open()");
        return -1;
    }

	// 終了完了報告 プロセスid送信
	if (write(fd_stop, buff, strlen(buff)) != strlen(buff))
	{
		DebugLogForDaemon("Cannot Send to fd_stop\n");
		//perror("write()");
		close(fd_stop);
		return -1;
	}
	
	DebugLogForDaemon("EventServer ended.\n");
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
// handler_1
//   名前付きパイプに対するハンドラ
//      pParm : ファイルディスクリプタに対するポインタ
///////////////////////////////////////////////////////////////////////////////
int handler_1(void* pParm)
{
	/*
	//printf("[%s] started\n", __FUNCTION__);

	int* pInt = (int*)pParm;
	int fd = (int)(*pInt);
	//printf("[%s] fd=%d\n", __FUNCTION__, fd);

	size_t stRead = 0;
	char buff[1024];
	memset(buff, 0, sizeof(buff));
	stRead = read(fd, buff, sizeof(buff));
	//printf("[%s] stRead=%d\n", __FUNCTION__, stRead);
	if ( stRead > 0 ) {
		// 表示用に改行文字を削除
		if ( buff[stRead-1] == '\n' ) {
			buff[stRead-1] = '\0';
		}
		//printf("[%s] data=[%s]\n", __FUNCTION__, buff);
	}

	// 終了指示を受信
	if ( strcmp(buff, "stop") == 0 ) {
		// ハンドラ削除
		bool bRet = true;
		bRet = mLoop.deleteHandler(fd);
		if ( bRet == false ) {
			//printf("deleteHandler error\n");
			// メインループを終了させる
			mLoop.exitMainLoop();
			return -1;
		}

		// メインループを終了させる
		mLoop.exitMainLoop();
	}
	*/
	return 0;
}

int handler_2(void* pParm)
{
	DebugLogForDaemon("handler_2 started\n");

	int* pInt = (int*)pParm;
	int fd = (int)(*pInt);
	size_t stRead = 0;
	char buff[1024];
	memset(buff, 0, sizeof(buff));

	stRead = read(fd, buff, sizeof(buff));
	if ( stRead <= 0 ) {
		DebugLogForDaemon("read error\n");
		return 0;
	}

	char work[256];
	sprintf(work, "buff=[%s]\n", buff);
	DebugLogForDaemon(work);

	if ( strcmp(buff, "recover_log") == 0 ) {
		recover_log();	
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// handler_timeout
//   タイムアウトに対するハンドラ
//      pParm : 
///////////////////////////////////////////////////////////////////////////////
int handler_timeout(void* pParm)
{
	DebugLogForDaemon("handler_timeout started\n");

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// サーバソケット(listen用)の作成
///////////////////////////////////////////////////////////////////////////////
bool createServerSock(int nPortNo, int& nSock)
{
	 // listen用sockaddrの設定
	struct sockaddr_in srcAddr;
	memset(&srcAddr, 0, sizeof(srcAddr));
	srcAddr.sin_port = htons(nPortNo);
	srcAddr.sin_family = AF_INET;
	srcAddr.sin_addr.s_addr = INADDR_ANY;

	// ソケットの生成(listen用)
	int srcSocket;
	srcSocket = socket(AF_INET, SOCK_STREAM, 0);
	if ( srcSocket == -1 ) {
		//printf("socket error\n");
		return -1;
	}

	// ソケットのバインド
	int nRet = 0;
	//nRet = std::bind(srcSocket, (struct sockaddr *)&srcAddr, sizeof(srcAddr));
	nRet = ::bind(srcSocket, (struct sockaddr *)&srcAddr, sizeof(srcAddr));//名前空間無しのbind
	if ( nRet == -1 ) {
		//printf("bind error\n");
		return -1;
	}

	// クライアントからの接続待ち
	nRet = listen(srcSocket, 1);
	if ( nRet == -1 ) {
		//printf("listen error\n");
		return -1;
	}

	nSock =  srcSocket;
	return true;
}

///////////////////////////////////////////////////////////////////////////////
// listen用のハンドラ
///////////////////////////////////////////////////////////////////////////////
int handler_listen(void* pParm)
{
	int* pSocket = (int*)pParm;
	int srcSocket = (int)(*pSocket);
	//printf("[%s] srcSocket=%d\n", __FUNCTION__, srcSocket);

	struct sockaddr_in dstAddr;
	int dstAddrSize = sizeof(dstAddr);
	int dstSocket = accept(srcSocket,
						(struct sockaddr *)&dstAddr, 
						(socklen_t *)&dstAddrSize);

	// ハンドラ登録
	bool bRet = mLoop.addHandler(dstSocket, handler_recv);
	if ( bRet == false ) {
		//printf("addHandler error\n");
		return -1;
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// recv用のハンドラ
///////////////////////////////////////////////////////////////////////////////
int handler_recv(void* pParm)
{
	bool bRet = true;

	int* pInt = (int*)pParm;
	int nSock = (int)(*pInt);

	//純粋なテキストメッセージ部分を保管する先頭アドレス
	char buf[MaxBufSize];

	//ヘッダーを含めた都度の受信メッセージ保管の先頭アドレス
	char bufForOnce[MaxBufSize];
	memset(buf, 0, sizeof(buf));

	int result = newCommonRcv(nSock, bufForOnce, buf);
	if(result == -1){
        close(nSock);

        // ハンドラ削除
		bRet = mLoop.deleteHandler(nSock);
		if ( bRet == false ) {
			return -1;
		}
    }

	for (int i=0; i< MaxBufSize; i++){ // bufの中の小文字を大文字に変換
		if ( isalpha(buf[i])) {
			buf[i] = toupper(buf[i]);
		}
	}

	// クライアントに返信
	if(sendFunc(buf, nSock) == -1){
        close(nSock);
        
        // ハンドラ削除
		bRet = mLoop.deleteHandler(nSock);
		if ( bRet == false ) {
			return -1;
		}
    }

	return 0;
}

///////////////////////////////////////////////////////////////////////////////
// シグナル用のハンドラ
///////////////////////////////////////////////////////////////////////////////
void sig_handler(int signo)
{
    //他のシグナルでも同じsig_handlerが呼ばれた場合用に分岐処理
  /*  
    FILE* fp = fopen("/tmp/sig_handler.txt", "a");
    fprintf(fp, "sig_handler started. signo=%d\n", signo);
    fclose(fp);
  */
	char work[256];
	sprintf(work, "sig_handler started. signo=%d\n", signo);
	DebugLogForDaemon(work);

    if (signo == SIGINT || signo ==  SIGTERM) {
		mLoop.exitMainLoop();	
    }
	return;
}

///////////////////////////////////////////////////////////////////////////////
// Logプロセスの復旧
///////////////////////////////////////////////////////////////////////////////
int recover_log(){

	char work[256];
	sprintf(work, "recover_log started\n");
	DebugLogForDaemon(work);

	sprintf(work, "pp[0]=%d\n", pp[0]);
	DebugLogForDaemon(work);

	//古い名前無しパイプ閉じる
	close(pp[0]);
	close(logpipe[1]);

	//(新)立ち上がり連絡用名前なしパイプ作成
	pipe(pp);
	char pp_buf[16];
	sprintf(pp_buf, "%d", pp[1]);//文字列に変換

	//(新)ログ用名前なしパイプ作成
	pipe(logpipe);
	char logpipe_buf[1024];
	sprintf(logpipe_buf, "%d", logpipe[0]);//文字列に変換

	//(新)ログプロセス起動
	pid_t e_pid = getpid(); //EventServerプロセスID取得
    char buff[16];
    sprintf(buff, "%d", e_pid);//文字列に変換

	pid_t pid = fork();

    if (pid == -1 ) 
	{
        DebugLogForDaemon("fork log failed in recover_log()\n");
		return -1;
    }
	else if (pid == 0) //子プロセスには0が返る
	{
		close(pp[0]); //名前無しパイプの読み込みディスクリプタをすぐ閉じる
		close(logpipe[1]);//ログ用名前なしパイプの書き込みをすぐ閉じる

		//複製した子プロセスをLogに化けさせる
		char* str[] = {(char*)log_path, buff, pp_buf, logpipe_buf, NULL};
		int nRet = execv(log_path, str);
        if ( nRet == -1 ) 
		{
            DebugLogForDaemon("log execv failed in EventServer.cpp\n");
			return -1;
        }
    }

	close(pp[1]); //名前無しパイプの書き込みディスクリプタを閉じる
	close(logpipe[0]);//ログ用名前なしパイプの読み込みディスクリプタをすぐ閉じる

	sprintf(work, "pp[0]=%d\n", pp[0]);
	DebugLogForDaemon(work);	

    //Logプロセス立ち上がり完了報告を読取
    bool flag = true;
	int nbyte;
	char buf[16];

    while (flag == true){
        if((nbyte = read(pp[0], buf, 16)) > 0){
			log_pid = atoi(buf);
            flag = false;
        }else if(nbyte == -1){
			//perror("read()");
			DebugLogForDaemon("Cannot read log pipe\n");
			close(pp[0]);
			return -1;
		}
    }

	return 0;
}
