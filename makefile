TARGET1 = Start
TARGET2 = Stop
TARGET3 = Observer
TARGET4 = EventServer
TARGET5 = TcpClient
TARGET6 = Log
CCFLAG = -std=c++11 -g

all : $(TARGET1) $(TARGET2) $(TARGET3) $(TARGET4) $(TARGET5) $(TARGET6)

$(TARGET1) : Start.o TcpCommon.o
	g++ -o $(TARGET1) Start.o TcpCommon.o -lrt

$(TARGET2) : Stop.o TcpCommon.o
	g++ -o $(TARGET2) Stop.o TcpCommon.o -lrt

$(TARGET3) : Observer.o TcpCommon.o
	g++ -o $(TARGET3) Observer.o TcpCommon.o -lrt

$(TARGET4) : EventServer.o CMainLoop.o TcpCommon.o
	g++ -o $(TARGET4) EventServer.o CMainLoop.o TcpCommon.o -lrt

$(TARGET5) : TcpClient.o TcpCommon.o
	g++ -o $(TARGET5) TcpClient.o TcpCommon.o -lrt

$(TARGET6) : Log.o TcpCommon.o
	g++ -o $(TARGET6) Log.o TcpCommon.o -lrt

Start.o : Start.cpp
	g++ -c $(CCFLAG) Start.cpp

Stop.o : Stop.cpp
	g++ -c $(CCFLAG) Stop.cpp

Observer.o : Observer.cpp
	g++ -c $(CCFLAG) Observer.cpp

EventServer.o : EventServer.cpp
	g++ -c $(CCFLAG) EventServer.cpp

TcpClient.o : TcpClient.cpp
	g++ -c $(CCFLAG) TcpClient.cpp

CMainLoop.o : CMainLoop.cpp CMainLoop.h
	g++ -c $(CCFLAG) CMainLoop.cpp

TcpCommon.o : TcpCommon.cpp TcpCommon.h
	g++ -c $(CCFLAG) TcpCommon.cpp

Log.o : Log.cpp
	g++ -c $(CCFLAG) Log.cpp

clean :
	rm -f $(TARGET1) $(TARGET2) $(TARGET3) $(TARGET4) $(TARGET5) $(TARGET6) *.o

