///////////////////////////////////////////////////////////////////////////////
//  Start Command
///////////////////////////////////////////////////////////////////////////////
#include <mqueue.h>
#include <signal.h>
#include <errno.h>
#include <err.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <arpa/inet.h>
#include "TcpCommon.h"
using namespace std;

int fd_stop = -1;//終了報告用fd;
int nbyte;
int nRet;
int closeStopPipe();
///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	char buff[256];
	pid_t pid;
	FILE *fp;
    struct mq_attr attr;
    mqd_t recv_que;

	//Observerプロセスが存在すればクローズ
	//fopenはwモードだと既存を破棄して新規作成になってしまうのでrモードで開く
	fp = fopen(PID_OBSERVER, "r");
	if(fp == NULL){
		printf("Open PID_OBSERVER File Failed!\n");
	}else{
		fgets(buff, 256, fp);
		printf("%s\n", buff);
		pid = atoi(buff);
		fclose(fp);
		//pidファイルを削除する
		unlink(PID_OBSERVER);

		//Observerプロセスのreadがエラーにならないようにメッセージキューをcreateしてからkill発行する
        recv_que = mq_open(QUE_NAME, O_RDONLY|O_CREAT, 0644, NULL);
        if(recv_que == -1) {
			perror("mq_open error\n");
		}

		kill(pid, SIGINT);

		//Observerプロセスの終了報告待ち
        mq_getattr(recv_que, &attr);//これやらないとmq_receiveでエラー

		if(recv_que != -1){
			if(mq_receive(recv_que, buff, attr.mq_msgsize, NULL) == -1) {
				//mq_receiveのパラメーター設定によってはメッセージがなかったら即リターンもできそう
				perror("mq_receive error\n");
			}
			printf("received msg: %s \n", buff);
			printf("close and unlink que\n");
			mq_close(recv_que);
			mq_unlink(QUE_NAME);
		}

	}

	//EventServerプロセスをクローズ
	//fopenはwモードだと既存を破棄して新規作成になってしまうのでrモードで開く
	fp = fopen(PID_SERVER, "r");
	if(fp == NULL){
		printf("Open PID File Failed!\n");
		return -1;
	}
	fgets(buff, 256, fp);
	printf("%s\n", buff);
	fclose(fp);
	pid = atoi(buff);

	// 入力パイプの作成
	unlink(PIPE_STOP);
	nRet = mkfifo(PIPE_STOP, 0666);
	if ( nRet ==-1 ) {
		perror("mkfifo\n");
		return -1;
	}
	
	kill(pid, SIGINT);

	//正常終了報告を名前付きパイプで受ける
    //読取専用でパイプを開く
    if ((fd_stop = open(PIPE_STOP, O_RDONLY)) == -1)
    {
		closeStopPipe();
        return 1;
    }

    //終了報告を読取
    bool flag = true;
    while (flag == true){
        if((nbyte = read(fd_stop, buff, strlen(buff))) > 0){
            //write(fileno(stdout), buff, nbyte);
			printf("EventServer Ended\n");
            flag = false;
        }else if(nbyte == -1){
			printf("EventServer did't Ended normally\n");
			perror("write()\n");
			closeStopPipe();
			return -1;
		}
    }

	closeStopPipe();
	return 0;
}

int closeStopPipe(){
	if(close(fd_stop) != 0) perror("close()\n");
    unlink(PIPE_STOP);
	return(0);
};
