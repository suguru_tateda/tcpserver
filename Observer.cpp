///////////////////////////////////////////////////////////////////////////////
//  Start Command
///////////////////////////////////////////////////////////////////////////////
#include <mqueue.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>
#include <syslog.h>
#include <err.h>
#include "TcpCommon.h"
#define MAXFD 64
using namespace std;

void sig_handler(int signo);
bool flag;
int  fd_req = -1;
int log_pid;
bool bRecoverE = false;
bool bRecoverL = false;
///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
	//pidファイルチェック
	int nRet = exist_process(PID_OBSERVER);
	if (nRet == -1) {
		DebugLogForDaemon("No Observer pid file\n");
		DebugLogForDaemon("Start Observer executed\n");
	}
	else if (nRet == 0) {
		DebugLogForDaemon("Observer pid file exist but process is no exist\n");
		DebugLogForDaemon("Start Observer executed\n");
	}
	else {
		DebugLogForDaemon("Observer process is already exist\n");
		DebugLogForDaemon("Start Observer Command stopped\n");
		return -1;
	}
	
	flag = true;
	int i = 0;
	int fd = 0;
	pid_t pid = 0;

	//------------------------------------------------------
	// 状態.
	// プログラム起動時は親プロセス(shell)があり、TTYを持っている.
	//------------------------------------------------------

	//------------------------------------------------------
	// 子プロセスを作成して親を切り離しシェルに制御を戻す.
	//------------------------------------------------------
	if ((pid = fork()) == -1) {
		DebugLogForDaemon("pif fork has failed in Observer.cpp\n");
		return -1;

		// 子プロセスを作成したら親プロセスは終了する.
	}
	else if (pid != 0) {

		// ユーザー定義のクリーンアップ関数を呼ばないように_exit()を行う.
		_exit(0);
	}


	//------------------------------------------------------
	// 状態.
	// 子プロセスのみとなったがTTYは保持したまま.
	//------------------------------------------------------


	//------------------------------------------------------
	// TTYを切り離してセッションリーダー化、プロセスグループリーダー化する.
	//------------------------------------------------------
	setsid();


	//------------------------------------------------------
	// HUPシグナルを無視.
	// 親が死んだ時にHUPシグナルが子にも送られる可能性があるため.
	//------------------------------------------------------
	signal(SIGHUP, SIG_IGN);


	//------------------------------------------------------
	// 状態.
	// このままだとセッションリーダーなのでTTYをオープンするとそのTTYが関連づけられてしまう.
	//------------------------------------------------------


	//------------------------------------------------------
	// 親プロセス(セッショングループリーダー)を切り離す.
	// 親を持たず、TTYも持たず、セッションリーダーでもない状態になる.
	//------------------------------------------------------
	if ((pid = fork()) != 0) {
		// 子プロセス終了.
		_exit(0);
	}


	//------------------------------------------------------
	// デーモンとして動くための準備を行う.
	//------------------------------------------------------

	// ルートディレクトリに移動.
	chdir("/");

#if 1
	// 親から引き継いだ全てのファイルディスクリプタのクローズ.
	for (i = 0; i < MAXFD; i++) {
		close(i);
	}

	// stdin,stdout,stderrをdev/nullでオープン.
	// 単にディスクリプタを閉じるだけだとこれらの出力がエラーになるのでdev/nullにリダイレクトする.
	if ((fd = open("/dev/null", O_RDWR, 0) != -1)) {

		// ファイルディスクリプタの複製.
		// このプロセスの0,1,2をfdが指すdev/nullを指すようにする.
		dup2(fd, 0);
		dup2(fd, 1);
		dup2(fd, 2);
		if (fd < 2) {
			close(fd);
		}
	}
#endif

	//------------------------------------------------------
	// デーモン化後の処理
	//------------------------------------------------------

	//エラー判定値(int)
	nRet = 0;

	// シグナルハンドラの設定
	struct sigaction act;
	memset(&act, 0, sizeof(act)); //メモリにゴミが入っているので初期化
	act.sa_handler = sig_handler;
	act.sa_flags = SA_RESTART; //何度シグナルが来てもハンドラ実行を許可する
	//act.sa_restorer = NULL; //廃止予定。POSIXでも規定されていない

//0 or 1で簡単に取り外し可能
#if 1
	// 割り込みを抑止するシグナルの設定
	sigset_t sigset; // シグナルマスク

	//シグナルマスクの初期化
	nRet = sigemptyset(&sigset);
	if (nRet != 0) return -1;

	//Control-Cで割り込まれないようにする
	nRet = sigaddset(&sigset, SIGUSR1);
	if (nRet != 0) return -1;
	act.sa_mask = sigset;
#endif
	//第1引数はシステムコール番号
	//第2引数は第1引数で指定したシステムコールで呼び出したいアクション
	//第3引数は第1引数で指定したシステムコールでこれまで呼び出されていたアクションが格納される。NULLだとこれまでの動作が破棄される
	nRet = sigaction(SIGINT,
		&act,
		NULL);
	//printf("sigaction nRet=%d\n", nRet);
	if (nRet == -1) err(EXIT_FAILURE, "sigaction error");

	//SIGTERMを補足
	memset(&act, 0, sizeof(act));
	act.sa_handler = sig_handler;
	nRet = sigaction(SIGTERM,
		&act,
		NULL);

	//PIDファイル作成
	pid_t o_pid = getpid(); //プロセスID取得
	char buff[16];
	sprintf(buff, "%d", o_pid);//文字列に変換
	if (write_pid(buff, PID_OBSERVER) == false) {
		//perror("write_pid()");
		DebugLogForDaemon("write PID_OBSERVER error\n");
		return -1;
	};

	//名前付きパイプ
	fd_req = open(REQUEST_PIPE, O_RDWR);
	if (fd_req == -1) {
		//perror("open REQUEST_PIPE");
		DebugLogForDaemon("open REQUEST_PIPE error\n");
		return -1;
	}

	DebugLogForDaemon("★★ Observerによる監視を開始します\n");
	while (flag) {
		sleep(10);
		
		int nExistEventServer = exist_process(PID_SERVER);
		int nExistLog = exist_process(PID_LOG);

		char work[256];
		sprintf(work, "nExistEventServer=%d, nExistLog=%d\n", nExistEventServer, nExistLog);
		DebugLogForDaemon(work);

		//if(nExistLog == 0){
		if(nExistLog == 1){
			//bRecoverL = false;
			if ( bRecoverL ) {
				DebugLogForDaemon("★★ Logの復旧が終了しました\n");
				bRecoverL = false;
			}
		}

		//if(nExistEventServer == 0){
		if ( nExistEventServer == 1 ){
			//bRecoverE = false;
			if ( bRecoverE ) {
				DebugLogForDaemon("★★ EventServerの復旧が終了しました\n");
				bRecoverE = false;
			}
		}

		if (nExistEventServer < 1) {
			if(bRecoverE){
				DebugLogForDaemon("recovering eventserver...\n");
				continue;
			}
			
			DebugLogForDaemon("★★ EventServerの復旧を開始します\n");
			bRecoverE = true;
			bRecoverL = false;

			//Logプロセスを終了
			log_pid = get_pid(PID_LOG);
			if (log_pid != -1) {
				nRet = kill(log_pid, SIGKILL);
				char work[256];
				sprintf(work, "kill nRet=%d, log_pid=%d\n", nRet, log_pid);
				DebugLogForDaemon(work);
			}

			//EventServerプロセスごと復旧
			//chdir(PROJ_HOME);
			//nRet = system("./Start 5000");

            // ゴミファイルの削除
            unlink(PID_SERVER);
            unlink(PID_LOG);

			char start_command[256];
			sprintf(start_command, "%s%s",PROJ_HOME, "/Start 5000");
			nRet = system(start_command);
			char result[256];
			sprintf(result, "system exitCode=%d\n", nRet >> 8);
			//sprintf(result, "result=%d\n", nRet);
			DebugLogForDaemon(result);
		}
		else if (nExistEventServer > 0 && nExistLog < 1) {
			if(bRecoverL){
				DebugLogForDaemon("recovering log....\n");
				continue;
			}

			DebugLogForDaemon("★★ Logの復旧を開始します\n");
			bRecoverL = true;

			fd_req = open(REQUEST_PIPE, O_RDWR);
			if (fd_req == -1) {
				DebugLogForDaemon("open REQUEST_PIPE error\n");
				return -1;
			}
			//EventServerプロセスに向けてLogプロセス復旧要求を書き込む
			char _buff[128];
			sprintf(_buff, "recover_log");
			size_t stRet = write(fd_req, _buff, strlen(_buff));
			if (stRet != strlen(_buff)) {
				DebugLogForDaemon("write error\n");
				return -1;
			}
		}

		//sleep(10);
	}

	DebugLogForDaemon("★★ Observerによる監視を終了します\n");

	//Stopコマンドに終了報告
	char msq_buff[256];
	mqd_t send_que;

	//stop側がcreateしていない場合はエラーになる
	send_que = mq_open(QUE_NAME, O_WRONLY);

	if (send_que == -1) {
		//perror("mq_open error");
		DebugLogForDaemon("mq_open error\n");
	}
	else {
		strcpy(msq_buff, "Observer has stopped!");
		if (mq_send(send_que, msq_buff, sizeof(msq_buff), 0) == -1) {
			//perror("error");
			DebugLogForDaemon("mq_send error\n");
		}
	}

	DebugLogForDaemon("Observer ended.\n");
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// シグナル用のハンドラ
///////////////////////////////////////////////////////////////////////////////
void sig_handler(int signo)
{
	//他のシグナルでも同じsig_handlerが呼ばれた場合用に分岐処理
	if (signo == SIGINT || signo == SIGTERM) {
		flag = false;
	}
	return;
}