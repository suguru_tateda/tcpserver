#pragma once
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>	
#include <string>
#include <string.h>

#define PIPE_START "/tmp/fifo_start"//Startに完了報告する名前付きパイプ
#define PIPE_STOP "/tmp/fifo_stop"//Stopに完了報告する名前付きパイプ

//EventServerが自ら書き込み&読み込みする
//ObserverからEventServerプロセスにLogプロセス復旧要求を書き込む
#define REQUEST_PIPE "/tmp/request_pipe"
#define PID_SERVER "/tmp/EventServer.pid"
#define PID_LOG "/tmp/Log.pid"
#define PID_OBSERVER "/tmp/Observer.pid"
#define TMP_LOGFILE "/tmp/log"
#define QUE_NAME "/mq_stop_observer"
#define PROJ_HOME "/vagrant/tcpserver"

enum class EDebugLv{
	__NOTICE,
	__DEBUG,
	__INFO,
	__ERROR
};

extern EDebugLv CurrentLogLv;

//ログ用ヘッダー
using LogStruct = struct {
	timespec  now;
	pid_t pid;
	//char* allocBuf;
	char allocBuf[1024];//文字数は要検討
};

//client - server 送受信データのヘッダー
const int _blockSize = 10;
const int MaxBufSize = 1024;

//通常typedefは定義のみで値の代入は行わない。
//最近のgccは定義&代入を許しているが昔のgccはコンパイルエラーになる
using head =  struct {
	int totalSize;
	int blockSize = _blockSize;
	int recordNo;
};


using MessStruct = struct {
	head _head;
	char _data[_blockSize];
};

//stdoutで標準出力してデバック
static void printHead(head* pHead, FILE* fp) {
	fprintf(fp,
			 "★★ header(totalSize:%d, blockSize:%d, recordNo:%d)\n",
			 pHead->totalSize,
			 pHead->blockSize,
			 pHead->recordNo);
	char* pData = (char*)pHead + sizeof(head);
	fprintf(fp,
			 "★★ data:[%s]\n",
			 pData);
	return;
}

int sendFunc(char*, int&);
int commonRcv(int& dstSocket, char* bufForOnce, size_t& stSize, char* bufForWholeMes, bool& flag);
int newCommonRcv(int& dstSocket, char* bufForOnce, char* bufForWholeMes);
int logFunc(int fifo, EDebugLv LogLv, pid_t pid, const char* fmt, ...);
bool write_pid(char* buff, std::string macro);
int DebugLogForDaemon(const char* message);
int exist_process (std::string macro);
int get_pid(std::string macro);