///////////////////////////////////////////////////////////////////////////////
//  TcpServer
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "TcpCommon.h"
///////////////////////////////////////////////////////////////////////////////
// main
///////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    ///////////////////////////////////
    // コマンド引数の解析
    ///////////////////////////////////
    if ( argc != 3 ) {
      printf("TcpClient IPAddress portNo\n");
      return -1;
    }

    char destination[32];   // 送信先IPアドレス
    int nPortNo;            // ポート番号
    strcpy(destination, argv[1]);
    nPortNo = atol(argv[2]);

    ///////////////////////////////////
    //sockaddr_in 構造体の作成
    ///////////////////////////////////
    struct sockaddr_in dstAddr;
    memset(&dstAddr, 0, sizeof(dstAddr));
    dstAddr.sin_family = AF_INET;
    dstAddr.sin_port   = htons(nPortNo);
    dstAddr.sin_addr.s_addr = inet_addr(destination);

    ///////////////////////////////////
    //ソケットの生成
    ///////////////////////////////////
    int dstSocket;
    dstSocket = socket(AF_INET, SOCK_STREAM, 0);
    if ( dstSocket == -1 ) {
        printf("ソケットの作成に失敗\n");
        return(-1);      
    }


    ///////////////////////////////////
    //　サーバに接続
    ///////////////////////////////////
    int nRet;
    nRet = connect(dstSocket,
                   (struct sockaddr *)&dstAddr,
                   sizeof(dstAddr));
    if ( nRet == -1 ) {
        printf("%s に接続できませんでした\n",destination);
        return(-1);
    }
    printf("%s に接続しました\n",destination);


    ///////////////////////////////////
    //　サーバとの通信
    ///////////////////////////////////
    while (1) {
        //純粋なテキストメッセージ部分を保管する先頭アドレス
        char buf[MaxBufSize];

        //ヘッダーを含めた都度の受信メッセージ保管の先頭アドレス
        char bufForOnce[MaxBufSize];

        printf("子文字のアルファベットを入力してください\n");
        scanf("%s",buf);
        
        if ( strcmp(buf, ".") == 0) {
            printf("処理を終了します\n");
            break;
        }

        //　データの送信
        if(sendFunc(buf, dstSocket) == -1){
          printf("send error.\n");
          break;
        }

        // サーバからの応答を受信
        int result = newCommonRcv(dstSocket, bufForOnce, buf);
        if(result == -1){
          printf("recv error.\n");
          break;
        }
        printf("→ %s\n",buf);
    }

    // ソケットをクローズ
    close(dstSocket);

    return(0);
}


