#include "CMainLoop.h"
#include <errno.h>
///////////////////////////////////////////////////////////////////////////////
// コンストラクタ
///////////////////////////////////////////////////////////////////////////////
CMainLoop::CMainLoop()
{
	//_count = 0;
	_nTimeout  = 0;
	_timeOutHandler = NULL;
	_bLoopExit = false;
}

///////////////////////////////////////////////////////////////////////////////
// デストラクタ
///////////////////////////////////////////////////////////////////////////////
CMainLoop::~CMainLoop()
{

}

///////////////////////////////////////////////////////////////////////////////
// ハンドラ登録
///////////////////////////////////////////////////////////////////////////////
bool CMainLoop::addHandler(int nEvent, EVHANDLEER handler)
{
	_eventMap[nEvent] = handler;
	return true;
}		

///////////////////////////////////////////////////////////////////////////////
// ハンドラ削除
///////////////////////////////////////////////////////////////////////////////
bool CMainLoop::deleteHandler(int nEvent)
{
	_eventMap.erase(nEvent);
	return true;
}
///////////////////////////////////////////////////////////////////////////////
// ハンドラ登録(タイムアウト用)
///////////////////////////////////////////////////////////////////////////////
bool CMainLoop::addTimeoutHandler(int nTimeout, EVHANDLEER handler)
{
	_nTimeout       = nTimeout;
	_timeOutHandler = handler;
	return true;
}	

///////////////////////////////////////////////////////////////////////////////
// メインループ
///////////////////////////////////////////////////////////////////////////////
bool CMainLoop::mainLoop()
{

	while( !_bLoopExit ) {
		// 待期するfdset1の作成
		fd_set  readfds;
		FD_ZERO(&readfds);

		eventMap::iterator ite;
		for ( ite=_eventMap.begin(); ite != _eventMap.end(); ite++) {
			FD_SET( ite->first, &readfds);
		}

		// タイムアウトの設定
		struct timeval  tval;
		tval.tv_sec  = _nTimeout	;	// time_t  秒
	    tval.tv_usec = 0;	// suseconds_t  マイクロ秒


		printf("Before select.\n");	
		int nRet = 0;
		nRet = select(FD_SETSIZE,
						&readfds,
						NULL,
						NULL,
						&tval );
		if( nRet == -1 ) {
			if(errno == EINTR){//シグナル割り込みは除外
				continue;
			}else{
				// selectが異常終了
				perror("select");
				return false;
			}
	  	}

	  	printf("select nRet=%d\n", nRet);

	  	// selectでタイムアウトが発生
	  	if ( nRet == 0) {
	  		// タイムアウト用ハンドラの呼出
	  		if ( _timeOutHandler != NULL) {
	  			_timeOutHandler(NULL);
	  		}
	  		continue;
	  	}

		///////////////////////////////////////
  		// 反応のあったディスクリプタをチェック
		///////////////////////////////////////
		for ( ite=_eventMap.begin(); ite != _eventMap.end(); ite++) {
			int fd = ite->first;
			if ( !FD_ISSET(fd, &readfds) ) {
				// 入力がないのでスキップ
				continue;
			}

			printf("fd=%d\n", fd);
			EVHANDLEER eHandler = ite->second;

			//関数ポインタの呼び出し
			eHandler(&fd);	
		}
	}



	return true;
}

///////////////////////////////////////////////////////////////////////////////
// メインループを終了させる
///////////////////////////////////////////////////////////////////////////////
bool CMainLoop::exitMainLoop()
{
	_bLoopExit = true;
	return true;
}
