#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <unistd.h>
#include "TcpCommon.h"
#include <syslog.h>
#include <string>

EDebugLv CurrentLogLv = EDebugLv::__DEBUG;
void createTimeStamp(std::string& strTime);

int sendFunc(char* buf, int& dstSocket){
	//restMessByteが0以下になってsendしきったかどうか
	bool loopFlag = true;

	//返信No初期化
	int recordNo = 1;

	MessStruct devidedMess = MessStruct();
	int restMessByte = strlen(buf)+1;//要返信バイト数。strlenは¥0を含まない。改行は含む

	while(loopFlag){
		devidedMess._head.totalSize = restMessByte;
		devidedMess._head.recordNo = recordNo;

		memcpy(devidedMess._data, (buf + _blockSize*(recordNo-1)), _blockSize);

		size_t szRet = send(dstSocket, &devidedMess, sizeof(MessStruct), 0);

		if( szRet == -1) return -1;

		//フラグとNo更新
		restMessByte -= _blockSize;
		if(restMessByte < 1) loopFlag = false;
		else recordNo++;
	}

	return 0;
}

int newCommonRcv(int& dstSocket, char* bufForOnce, char* bufForWholeMes){
	//flag:予定総受信バイト数に現受信バイト数が届いているかどうか
	bool flag = false;
	size_t stSize = MaxBufSize;

	while(flag == false){
		size_t _stSize= recv(dstSocket,
						bufForOnce,
						stSize,
						0);

		if ( _stSize == 0 || _stSize == -1) {
			return -1;
		}

		//受信したバッファの総レコード数
		int nReceivedRecCount = _stSize/sizeof(MessStruct);
		int nRecordCount = 0;

		//総レコード数分回す
		while( nRecordCount < nReceivedRecCount) {     
			//////////////////////////////////////////////////////////
			/////受信メッセージからヘッダーを取って実メッセージ連結する処理/////
			//////////////////////////////////////////////////////////

			//ヘッダーの番地を特定
			MessStruct* pDevidedMess = (MessStruct*)(bufForOnce + sizeof(MessStruct)*nRecordCount);
			nRecordCount++;

			int recordNo = pDevidedMess->_head.recordNo;

			//第一引数:コピー先アドレス、第二引数:コピー元アドレス
			strcpy(bufForWholeMes + _blockSize * (recordNo-1), pDevidedMess->_data);

			//現在の受信バイト数が総バイト数より小さければ
			if(recordNo * _blockSize < pDevidedMess->_head.totalSize) flag = false;
			else flag = true;
		}
	}

	return(0);
}

int logFunc(int fifo, EDebugLv LogLv, pid_t pid, const char* fmt, ...){
	//ログレベル確認
	if(static_cast<int>(CurrentLogLv) > static_cast<int>(LogLv))
	{
		return(0);
	}

	//フォーマット文字列処理
	va_list ap;
	char* allocBuf;//連結後の文字列

	va_start(ap, fmt);
	int nSize = vasprintf(&allocBuf, fmt, ap);
	va_end(ap);

	//タイムスタンプ
	timespec  now;
	clock_gettime(CLOCK_REALTIME, &now);

	//ログインスタンス生成
	LogStruct Log;

	//初期化
	Log.now = now;
	Log.pid = pid;
	memset(&(Log.allocBuf), 0, sizeof(Log.allocBuf));//0クリア

	//strncpy(Log.allocBuf, allocBuf, sizeof(Log.allocBuf));/フォーマット文字数以上のメモリ領域にアクセスしてしまい落ちてしまう
	strncpy(Log.allocBuf, allocBuf, nSize + 1);//vasprintfの戻り値は\nを含まない文字数
	//strncpy(Log.allocBuf, allocBuf, strlen(allocBuf)+1);//strlenの戻り値は\nを含まない文字数

	//名前付きパイプでwrite
    if (write(fifo, &Log, sizeof(Log)) != sizeof(Log))//writeが失敗したら
	{
        //perror("write()");
		DebugLogForDaemon("write log fifo failed in TcpCommon\n");

		//エラーになっても解放を忘れない
		free(allocBuf);

		//メイン関数へ-1を返す。メイン関数側では−１をフックにclose(fifo);
        return -1;
    }

	//正常終了でも解放
	free(allocBuf);

	return(0);
};

bool write_pid(char* buff, std::string macro){
  FILE *fp;
  fp = fopen(macro.c_str(), "w");
  if(fp == NULL){
	  return false;
  }
  fprintf(fp, "%s", buff);
  fclose(fp);

  return true;
}

int DebugLogForDaemon(const char* message){

  std::string strTimeStamp;
  createTimeStamp(strTimeStamp);

  FILE *fp;
  fp = fopen(TMP_LOGFILE, "a");
  if(fp == NULL){
	  return -1;
  }
  fprintf(fp, "[%s][%d] %s", strTimeStamp.c_str(), getpid(), message);
  fflush(fp);
  fclose(fp);
  return 0;
}

void createTimeStamp(std::string& strTime)
{
	struct timespec  now;
	clock_gettime(CLOCK_REALTIME, &now);
	struct tm local;
	localtime_r(&now.tv_sec, &local);

	char timebuff[32];
	sprintf(timebuff ,
			"%04d/%02d/%02d %02d:%02d:%02d.%03ld",
			local.tm_year + 1900,
			local.tm_mon + 1,
			local.tm_mday,
			local.tm_hour,
			local.tm_min,
			local.tm_sec,
			now.tv_nsec / 1000000 );
	//printf("%s\n", timebuff);
	strTime = timebuff;
	return;
}

///////////////////////////////////////////////////////////////////////////////
// プロセス生存確認
///////////////////////////////////////////////////////////////////////////////
int exist_process (std::string macro)
{
	char buff[256];
	pid_t pid = get_pid(macro);
	if(pid == -1){
		sprintf(buff, "pid file is no exist. (%s)\n", macro.c_str());
		DebugLogForDaemon(buff);
		return -1;
	}
	sprintf(buff, "%d", pid);

	//プロセス生存確認
	int r = kill (pid, 0);
	if (r == 0)
	{
		// pid は存在する
		std::string message = std::string(buff);
		message += " is exist!\n";//文字列連結
		char *cstr = new char[message.length() + 1];//char領域メモリを新規作成
		strcpy(cstr, message.c_str());//コピー
		//sprintf(buff, "pid file is no exist. (%s)\n", macro.c_str());
		DebugLogForDaemon(cstr);
		delete [] cstr;//メモリ解放
		return (1);
	}

	// pid は存在しない
	std::string message = std::string(buff);
	message += " is no exist!\n";//文字列連結
	char *cstr = new char[message.length() + 1];//char領域メモリを新規作成
	strcpy(cstr, message.c_str());//コピー
	DebugLogForDaemon(cstr);
	delete [] cstr;//メモリ解放
	return (0);
}

int get_pid(std::string macro){
	char buff[256];
	pid_t pid;
	FILE *fp;

	//fopenはwモードだと既存を破棄して新規作成になってしまうのでrモードで開く
	fp = fopen(macro.c_str(), "r");
	if(fp == NULL){
		//perror("Open PID File Failed!\n");
		DebugLogForDaemon("Open PID File Failed!\n");
		return -1;
	}
	fgets(buff, 256, fp);
	pid = atoi(buff);
	fclose(fp);
	
	return pid;
}