#include <signal.h>
#include <errno.h>
#include <err.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "TcpCommon.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string>
#include <string.h>
void sig_handler(int signo);

bool loop_flag = true;
char ch_time[1024];
char ch_message[1024];
char ch_pid[256];

int main(int argc, char *argv[])
{
    char work[256];
    int e_pid;
    //EventServerからプロセスidを受け取る
    e_pid = atoi(argv[1]);

    //立ち上がり報告用名前無しパイプの書き込みディスクリプタ番号(書き込み専用)
    int pp;
    pp = atoi(argv[2]);

    //ログ用名前無しパイプの書き込みディスクリプタ番号(読み込み専用)
    int logpipe;
    logpipe = atoi(argv[3]);

    pid_t l_pid = getpid(); //LogプロセスID取得
    char buf[16];
	sprintf(buf, "%d", l_pid);//文字列に変換
    

    sprintf(work,
             "epid=%d, pp=%d, logpie=%d\n",
            e_pid, pp, logpipe);
    DebugLogForDaemon(work);

    ///////////////////////////////////////////////////////////////////////////////
    // シグナル設定
    ///////////////////////////////////////////////////////////////////////////////
	//エラー判定値(int)
	int nRet = 0;

    // シグナルハンドラの設定
    struct sigaction act;
    memset(&act, 0, sizeof(act)); //メモリにゴミが入っているので初期化
    act.sa_handler = sig_handler;
    //act.sa_flags = SA_RESTART; //何度シグナルが来てもハンドラ実行を許可する
    //act.sa_restorer = NULL; //廃止予定。POSIXでも規定されていない

//0 or 1で簡単に取り外し可能
#if 1
    // 割り込みを抑止するシグナルの設定
    sigset_t sigset; // シグナルマスク
    
    //シグナルマスクの初期化
    nRet = sigemptyset(&sigset);
    if( nRet != 0 ){
        close(pp);
        close(logpipe);
        return -1;
    }

    //Control-Cで割り込まれないようにする
    nRet = sigaddset(&sigset, SIGINT);
    if( nRet != 0 ){
        DebugLogForDaemon("sigaddset error\n");
        close(pp);
        close(logpipe);
        return -1;
    }
    act.sa_mask = sigset;
#endif
    //第1引数はシステムコール番号
    //第2引数は第1引数で指定したシステムコールで呼び出したいアクション
    //第3引数は第1引数で指定したシステムコールでこれまで呼び出されていたアクションが格納される。NULLだとこれまでの動作が破棄される
/*
    memset(&act, 0, sizeof(act));
    act.sa_handler = sig_handler;
    sigemptyset(&sigset);
    act.sa_mask    = sigset;
*/
    nRet = sigaction(SIGUSR1,
                    &act,
                    NULL);
    if ( nRet == -1 ){
        DebugLogForDaemon("sigaction error in Log.cpp\n");
        close(pp);
        close(logpipe);
        //err(EXIT_FAILURE, "sigaction error");
    }


	//SIGTERMを補足
	memset(&act, 0, sizeof(act));
	act.sa_handler = sig_handler;
	nRet = sigaction(SIGTERM,
		&act,
		NULL);

	//PIDファイル作成
	if(write_pid(buf, PID_LOG) == false){
        DebugLogForDaemon("write PID_LOG error\n");
        close(pp);
        close(logpipe);
		//perror("write_pid()");
		return -1;
	};
    
    ///////////////////////////////////////////////////////////////////////////////
    // 名前無しパイプへ立ち上がり完了報告の書き込み
    ///////////////////////////////////////////////////////////////////////////////
    if(write(pp, buf, strlen(buf) + 1) != strlen(buf) + 1){
        DebugLogForDaemon("write pp error\n");
        //perror("write()");
        close(pp);
        close(logpipe);
        return -1;
    }

    ///////////////////////////////////////////////////////////////////////////////
    // 立ち上がり後の処理
    ///////////////////////////////////////////////////////////////////////////////

    //openlog("EventServer", LOG_PID, LOG_USER);
    //syslog(LOG_NOTICE, log);
    //syslog(LOG_DEBUG, log);
    //syslog(LOG_INFO, format_str,0);
    //closelog();

    int nbyte;
    LogStruct log_buf;

    DebugLogForDaemon("Log: start main loop\n");

    while(loop_flag){
        //DebugLogForDaemon("Log: before read\n");
        
        //if((nbyte = read(logpipe, &log_buf, sizeof(log_buf))) > 0){
        nbyte = read(logpipe, &log_buf, sizeof(log_buf));
        sprintf(work, "Log nbyte=%d\n", nbyte);
        //DebugLogForDaemon(work);

        if( nbyte  > 0 ) {
            //timespec
            struct timespec ts = log_buf.now;
            //strftime(ch_time, sizeof ch_time, "%D %T", gmtime(&ts.tv_sec));

            //プロセスid
            pid_t pid = log_buf.pid;
            sprintf(ch_pid, "%d", pid);//文字列に変換

            //ログテキスト
            strncpy(ch_message, log_buf.allocBuf, strlen(log_buf.allocBuf)+1);

        }else if(nbyte == -1 && errno != EINTR){
			//perror("read()");
            DebugLogForDaemon("read logpipe error\n");
			close(logpipe);
			return -1;
		}
    }

    ///////////////////////////////////////////////////////////////////////////////
    // 終了処理
    ///////////////////////////////////////////////////////////////////////////////
	//自分のPIDファイルをunlink
	unlink(PID_LOG);
    sprintf(work, "before write. \n");
    DebugLogForDaemon(work);

    if(write(pp, buf, strlen(buf) + 1) != strlen(buf) + 1){
        //perror("write()");
        DebugLogForDaemon("write pp error\n");
        close(pp);
        close(logpipe);
        return -1;
    }
    close(pp);
    close(logpipe);
    
    DebugLogForDaemon("Log ended.\n");
    return 0;
}

///////////////////////////////////////////////////////////////////////////////
// シグナル用のハンドラ
///////////////////////////////////////////////////////////////////////////////
void sig_handler(int signo)
{
    char work[256];
    sprintf(work, "sig_handler(Log) started. sigNo=%d\n", signo);
    DebugLogForDaemon(work);

    //他のシグナルでも同じsig_handlerが呼ばれた場合用に分岐処理
    if (signo == SIGUSR1 || signo == SIGTERM) {
		loop_flag = false;
    }
    
    return;
}